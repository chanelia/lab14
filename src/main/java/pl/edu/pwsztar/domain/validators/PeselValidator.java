package pl.edu.pwsztar.domain.validators;


public class PeselValidator {

    public static boolean isValid(final String pesel) {

        if(pesel.length()!=11){
            return false;
        }

        int[] weight = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int sum = 0;
        int controlNumber = Integer.parseInt(pesel.substring(10, 11));

        for (int i = 0; i < weight.length; i++) {
            sum += (Integer.parseInt(pesel.substring(i, i + 1)) * weight[i]);
        }
        sum = sum % 10;
        return (10 - sum) % 10 == controlNumber;
    }
}
